<?php
/**
* @project    Atom-M CMS
* @package    API \Module
* @url        http://dev.atom-m.net
* @authors    MrBoriska(Boris Lapin)
*/


namespace ApiModule;

class ActionsHandler extends \Module {


    public $module = 'api';

    public $template = 'api';

    // Отключаем ведение статистики
    public $counter = false;
    // И кеширование(пока)
    public $cached = false;
    // По умолчанию контроль аргументов включен
    public $not_control_args = false;

    function __construct($params) {

        $this->params = $params;

        // Выключаем контроль аргументов для методов conf и access
        if (in_array($params[1],array('conf','access')))
            $this->not_control_args = true;
    }


    public function index($name = 'main') {
        parent::__construct($this->params);

        if (!file_exists(R.'template/'.getTemplate().'/html/'.$this->template.'/'.$name.'.html')) {
            $_GET['ac'] = 404;
            include_once R.'sys/error.php';
            return 0;
        }

        echo $this->render($name.'.html');
        return true;
    }

    /** get database */
    public function data($table,$id = false) {

        if (in_array('data/'.$table,explode(',',\Config::read('api.bad_requests'))))
            return $this->json_view(array('error' => 'Bad request'));

        $entity = false;

        $params = array();
        $params['cond'] = array();

        try {
            if (\Config::read('api.orm_mode'))
                $modelName = \OrmManager::getModelName($table);

            if ((\Config::read('api.orm_mode') && class_exists($modelName)) || !\Config::read('api.orm_mode')) {

                if (\Config::read('api.orm_mode'))
                    $model = new $modelName;

                // Формируем ID, IDS или другие значения для поиска
                if ($id !== false) {
                    if (is_numeric($id) && mb_strpos($id,'.') === false) {
                        $params['cond'] = array('id' => $id);
                    } else {
                        $ids = explode('.',$id);
                        if (count($ids) > 1) {
                            if (is_numeric(end($ids)))
                                $params['cond'] = array('`id` IN (' . implode(',',$ids) . ')');
                            else {
                                $name = array_pop($ids);
                                if (count($ids) > 1)
                                    $params['cond'] = array('`'.$name.'` IN (' . implode(',',$ids) . ')');
                                else
                                    $params['cond'] = array($name => $ids[0]);
                            }
                        } else {
                            return $this->json_view(array('error' => 'Bad request'));
                        }
                    }
                }

                // Устанавливаем режим
                if (isset($_GET['type']) && in_array($_GET['type'], array('DB_FIRST', 'DB_ALL', 'DB_COUNT')))
                    $DB_TYPE = $_GET['type'];
                else
                    $DB_TYPE = 'DB_ALL';


                // Устанавливаем параметры
                if (isset($_GET['sort'])) {
                    $sort = explode('|',$_GET['sort']);
                    if (preg_match('#^[-_0-9A-Za-z]+$#ui', $sort[0])) {
                        if (!in_array($sort[1],array('DESC','ASC'))) $sort[1] = 'DESC';
                        $params['order'] = '`'.$sort[0].'` '.$sort[1];
                    }
                }
                if (isset($_GET['limit']) && is_numeric($_GET['limit']) && mb_strpos($_GET['limit'],'.') === false) $params['limit'] = $_GET['limit'];
                if (isset($_GET['page']) && is_numeric($_GET['page']) && mb_strpos($_GET['page'],'.') === false) $params['page'] = $_GET['page'];
                if (isset($_GET['fields'])) $params['fields'] = explode('|',$_GET['fields']);

                if (\Config::read('api.orm_mode')) {
                    switch($DB_TYPE) {
                        case 'DB_FIRST':
                            $params['limit'] = 1;
                            $entity = $model->getCollection($params['cond'],$params);
                            break;
                        case 'DB_COUNT':
                            $entity = $model->getTotal($params);
                            break;
                        case 'DB_ALL':
                            $entity = $model->getCollection($params['cond'],$params);
                            break;
                    }
                } else
                    $entity = getDB()->select($table, constant($DB_TYPE), $params);
            }

            if ($entity !== false) {
                if ($DB_TYPE !== DB_COUNT && \Config::read('api.orm_mode')) {
                    // Запускаем метод \ORM::__getAPI() чтобы получить данные для API
                    $apis = array();
                    foreach($entity as $n => $obj) {
                        if (is_object($obj)) {
                            if (method_exists($obj,'__getAPI')) {
                                $__getAPI = $obj->__getAPI();
                                if (!empty($__getAPI)) {
                                    $apis[$n] = $__getAPI;
                                    foreach($apis[$n] as $field => $val) {
                                        if (empty($val)) {
                                            unset($apis[$n][$field]);
                                        }
                                    }
                                }
                            } else {
                                return $this->json_view(array('error' => 'This entity does not supported API'));
                                break;
                            }
                        } else {
                            if (!empty($obj)) {
                                $apis[$n] = $obj;
                            }
                        }
                    }
                    // Восстанавливаем нумерацию
                    $apis = array_values($apis);

                } else {
                    $apis = $entity;
                }
            } else {
                return $this->json_view(array('error' => 'This entity not found'));
            }
        } catch (Exception $e) {
            return $this->json_view(array('error' => $e->getMessage()));
        }


        // Выводим результат
        return $this->json_view($apis);
    }

    /** get settings into config */
    public function conf() {

        $params = func_get_args();

        if (in_array('conf/'.$params[0],explode(',',\Config::read('api.bad_requests'))))
            return $this->json_view(array('error' => 'Bad request'));


        if (!empty($params) && $params[1] == 'all') {
            return $this->json_view(\Config::read('all',$params[0]));
        }
        if (empty($params) || $params[0] == 'all') {
            return $this->json_view(\Config::read('all'));
            unset($apis['__db__']);
        }


        return $this->json_view(\Config::read(implode('.',$params)));


    }

    /** check access */
    public function access() {

        $params = func_get_args();

        if (in_array('conf/'.$params[0],explode(',',\Config::read('api.bad_requests'))))
            return $this->json_view(array('error' => 'Bad request'));


        // Выводим результат
        return $this->json_view(\ACL::turnUser($params));
    }

    /** Print json */
    private function json_view($data) {

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        return true;
    }


}

