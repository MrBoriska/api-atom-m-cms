<?php

$settingsInfo = array(
    'title' => array(
        'title' => __('Title'),
        'description' => __('Title: info'),
    ),
    'description' => array(
        'title' => __('Meta-Description'),
        'description' => __('Meta-Description: info'),
    ),
    'orm_mode' => array(
        'type' => 'checkbox',
        'title' => __('ORM mode'),
        'description' => __('ORM mode: info'),
    ),
    'bad_requests' => array(
        'title' => __('bad_requests'),
        'description' => __('bad_requests: info'),
        'onsave' => array(
            'func' => function($tmpSet, $fname) {
                $arr = explode(",",$tmpSet[$fname]);
                foreach($arr as $n => $val) {
                    $arr[$n] = trim(trim($val),'/');
                }
                $tmpSet[$fname] = implode(',',$arr);
                return $tmpSet[$fname];
            }
        ),
    ),
    'active' => array(
        'type' => 'checkbox',
        'title' => __('Module status'),
        'description' => __('Module status: info'),
    ),
);
